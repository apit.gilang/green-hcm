//
//  GreenHcmAPI.swift
//  GreenHcm
//
//  Created by Apit Gilang Aprida on 26/09/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

typealias SuccessResponse = (JSON?) -> Void
typealias ErrorResponse  = (String) -> Void

class GreenHcmAPI {
    
    static let instance = GreenHcmAPI()
    
    var alamoFireManager: SessionManager = SessionManager.default
    var req: Request?
    
    init() {
        self.setAFconfig()
    }
    
    func setAFconfig() {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 30
        configuration.timeoutIntervalForResource = 30
        self.alamoFireManager = Alamofire.SessionManager(configuration: configuration)
    }
    
    func request(_ request: URLRequestConvertible, success:@escaping SuccessResponse, error:@escaping ErrorResponse) {
        req = alamoFireManager.request(request).responseJSON { response in
            #if DEBUG
            if let errorMessage = response.result.error?.localizedDescription {
                print(errorMessage)
            }
            #endif
            
            let statusCode = response.response?.statusCode ?? 500
            if response.result.isFailure || 500 == response.response?.statusCode {
                if let reachability = Reachability(), reachability.isReachable == false {
                    error(Response.MESSAGE_NOT_CONNECTED_INTERNET)
                } else {
                    error(Response.MESSAGE_SYSTEM_ERROR)
                }
            } else if let value = response.result.value {
                let jsonValue = JSON(value)
                print(jsonValue)
                if statusCode >= 300 {
                    let message = jsonValue["message"].stringValue
                    if message.isEmpty {
                        error(Response.MESSAGE_SYSTEM_ERROR)
                    } else {
                        error(message)
                    }
                } else {
                    success(jsonValue)
                }
            } else {
                error(Response.MESSAGE_SYSTEM_ERROR)
            }
            
            //            self.invalidateAndConfigure()
        }
    }
    
    func uploadImage(_ request: URLRequestConvertible, multipartFormData: @escaping (MultipartFormData) -> Void, success: @escaping SuccessResponse, error:@escaping ErrorResponse) {
        alamoFireManager.upload(multipartFormData: multipartFormData, with: request) { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON {
                    response in
                    if response.result.error == nil {
                        let json = JSON(response.result.value!)
                        #if DEBUG
                        print(json)
                        #endif
                        success(json)
                    } else {
                        error(Response.MESSAGE_NOT_CONNECTED_INTERNET)
                    }
                }
            case .failure(let encodingError):
                #if DEBUG
                print(encodingError)
                #endif
                error(Response.MESSAGE_NOT_CONNECTED_INTERNET)
            }
            self.invalidateAndConfigure()
        }
    }
    
    func invalidateAndConfigure() {
        self.alamoFireManager.session.finishTasksAndInvalidate()
        self.setAFconfig()
    }
    
    class Response {
        static let MESSAGE_SYSTEM_ERROR = "Maaf, terjadi kesalahan sistem"
        static let MESSAGE_INTERNAL_SERVER_ERROR = "Terjadi Kesalahan Server"
        static let MESSAGE_NOT_CONNECTED_INTERNET = "Tidak Ada Koneksi Internet"
        static let MESSAGE_VERIFY_PHONE = "Mohon verifikasi nomor ponsel Anda."
        static let MESSAGE_FAIL_SEND_CODE = "Gagal mengirimkan kode verifikasi"
        static let MESSAGE_FAIL_VALIDATION_CODE = "Gagal validasi kode"
        static let MESSAGE_FAIL_ENCRYPT_PASSWORD = "Gagal Mengenkripsi Kata Sandi"
        
        static func shouldHiddenWarningImage(withMessage message: String?) -> Bool {
            return MESSAGE_INTERNAL_SERVER_ERROR == message || MESSAGE_NOT_CONNECTED_INTERNET == message
        }
        
        static func isMessageError(withMessage message: String?) -> Bool {
            return MESSAGE_SYSTEM_ERROR == message
        }
        
        static func isMessageNotConnectedInternet(withMessage message: String?) -> Bool {
            return MESSAGE_NOT_CONNECTED_INTERNET == message
        }
    }
}
