//
//  OnboardingViewController.swift
//  GreenHcm
//
//  Created by Fivecode on 23/09/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import Foundation
import UIKit

// MARK: - SegueConstants

enum SegueConstantOnboarding {
    // TODO: Add segue ids
    static let goToLogin = "goToLogin"
}

// MARK: -  OnboardingViewController

class OnboardingViewController: ViewController {
    
    // MARK: Properties
    

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var displayImage: UIImageView!
    @IBOutlet weak var displayTitle: UILabel!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var displayText: UITextView!
    var index: Int?
    var pageControlLength: Int?
    var currentViewControllerIndex = 0
    var presenter: OnboardingPresenter!
    
    let onboardingTitleList = ["Sistem Sumber Daya Manusia", "Sistem Manajemen Tugas","Obrolan dan Pemberitahuan Langsung","Sistem Pendukung"]
    let onBoardingTextList = ["Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."]
    let onBoardingImage = ["LEARN1", "LEARN2", "LEARN3", "LEARN4"]
    
    // MARK: Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        OnboardingPresenter.config(withOnboardingViewController: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configurePageViewController()
    }
    
    func configurePageViewController() {
        pageControl.numberOfPages = onBoardingTextList.count
        pageControl.currentPage = currentViewControllerIndex
        
        //MARK: 1 - Get reference to OnboardingPageViewController
        guard let pageViewController = storyboard?.instantiateViewController(withIdentifier: String(describing: onBoardingPageViewController.self)) as? onBoardingPageViewController else {
            return
        }
        
        //MARK: 2 - Assign the delegate and the data source
        pageViewController.delegate = self
        pageViewController.dataSource = self
        
        //MARK: 3 - Add as a child view controller
        addChild(pageViewController)
        pageViewController.didMove(toParent: self)
        
        //MARK: 4 - Placing pageViewController view into the contentView
        contentView.addSubview(pageViewController.view)
        
        //MARK: 5 - Make view dictionary
        let _ : [String: Any] = ["pageView": pageViewController.view as Any]
        
        //MARK: 6 - Starting view controller
        detailViewControllerAt(index: currentViewControllerIndex)
        
    }
    
    func detailViewControllerAt(index: Int) -> OnboardingViewController? {
        
        if index >= onboardingTitleList.count || onboardingTitleList.count == 0 {
            return nil
        }
        
        self.index = index
        self.displayTitle.text = onboardingTitleList[index]
        self.displayText.text = onBoardingTextList[index]
        self.pageControlLength = onBoardingTextList.count
        self.displayImage.image = UIImage(named: onBoardingImage[index])
        pageControl.currentPage = index
        
        return self
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if SegueConstantOnboarding.goToLogin == segue.identifier {
            if let nav = segue.destination as? UINavigationController {
                if let _ = nav.viewControllers.first as? LoginViewController {
                    
                }
            }
        }
    }
    
    func goToLoginView(){
        performSegue(withIdentifier: SegueConstantOnboarding.goToLogin, sender: nil)
    }
    
    @IBAction func nextBtnClicked(_ sender: UIButton) {
        if currentViewControllerIndex + 1 == onBoardingTextList.count {
            goToLoginView()
        } else {
            currentViewControllerIndex += 1
            configurePageViewController()
        }
    }
    
    @IBAction func skipBtnClicked(_ sender: UIButton) {
        goToLoginView()
    }
}

// MARK: - OnboardingView

extension OnboardingViewController: OnboardingView, UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard var currentIndex = index else {
            return nil
        }
        
        if currentIndex == 0 {
            return nil
        }
        
        currentIndex -= 1
        currentViewControllerIndex = currentIndex
        
        return detailViewControllerAt(index: currentIndex)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        guard var currentIndex = index else {
            return nil
        }
        
        if currentIndex == onboardingTitleList.count {
            return nil
        }
        
        currentIndex += 1
        currentViewControllerIndex = currentIndex
        
        return detailViewControllerAt(index: currentIndex)
    }
    
    // TODO: implement view methods
}
