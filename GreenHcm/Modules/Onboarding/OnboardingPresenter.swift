//
//  OnboardingPresenter.swift
//  GreenHcm
//
//  Created by Fivecode on 23/09/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol OnboardingViewPresenter: class {
    init(view: OnboardingView)
    // TODO: Declare view presenter methods
}

protocol OnboardingView: class {
    // TODO: Declare view methods
}

class OnboardingPresenter: OnboardingViewPresenter {
    
    static func config(withOnboardingViewController vc: OnboardingViewController) {
        let presenter = OnboardingPresenter(view: vc)
        vc.presenter = presenter
    }
    
    let view: OnboardingView
    
    required init(view: OnboardingView) {
        self.view = view
    }
    
    // TODO: Implement view presenter methods
}
