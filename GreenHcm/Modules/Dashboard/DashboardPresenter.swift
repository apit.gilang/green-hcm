//
//  DashboardPresenter.swift
//  GreenHcm
//
//  Created by Apit on 9/25/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol DashboardViewPresenter: class {
    init(view: DashboardView)
    // TODO: Declare view presenter methods
}

protocol DashboardView: class {
    // TODO: Declare view methods
}

class DashboardPresenter: DashboardViewPresenter {
    
    static func config(withDashboardViewController vc: DashboardViewController) {
        let presenter = DashboardPresenter(view: vc)
        vc.presenter = presenter
    }
    
    let view: DashboardView
    
    required init(view: DashboardView) {
        self.view = view
    }
    
    // TODO: Implement view presenter methods
}
