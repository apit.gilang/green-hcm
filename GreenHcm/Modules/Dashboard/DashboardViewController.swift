//
//  DashboardViewController.swift
//  GreenHcm
//
//  Created by Apit on 9/25/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import Foundation
import UIKit

// MARK: - SegueConstants

enum SegueConstantDashboard {
    // TODO: Add segue ids
}

// MARK: -  DashboardViewController

class DashboardViewController: ViewController {
    
    // MARK: Properties
    
    var presenter: DashboardPresenter!
    
    // MARK: Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        DashboardPresenter.config(withDashboardViewController: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

// MARK: - DashboardView

extension DashboardViewController: DashboardView {
    // TODO: implement view methods
}
