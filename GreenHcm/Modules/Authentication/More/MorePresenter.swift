//
//  MorePresenter.swift
//  GreenHcm
//
//  Created by Fivecode on 24/09/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol MoreViewPresenter: class {
    init(view: MoreView)
    // TODO: Declare view presenter methods
}

protocol MoreView: class {
    // TODO: Declare view methods
}

class MorePresenter: MoreViewPresenter {
    
    static func config(withMoreViewController vc: MoreViewController) {
        let presenter = MorePresenter(view: vc)
        vc.presenter = presenter
    }
    
    let view: MoreView
    
    required init(view: MoreView) {
        self.view = view
    }
    
    // TODO: Implement view presenter methods
}
