//
//  MoreViewController.swift
//  GreenHcm
//
//  Created by Fivecode on 24/09/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import Foundation
import UIKit

// MARK: - SegueConstants

enum SegueConstantMore {
    // TODO: Add segue ids
}

// MARK: -  MoreViewController

class MoreViewController: ViewController {
    
    // MARK: Properties
    
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var arrowDownView: UIView!
    
    var presenter: MorePresenter!
    
    // MARK: Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        MorePresenter.config(withMoreViewController: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cardView.roundCorners(cornerRadius: 10)
        arrowDownView.roundCorners(cornerRadius: 100)
    }
    @IBAction func arrowBtnClicked(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
}

// MARK: - MoreView

extension MoreViewController: MoreView {
    // TODO: implement view methods
}
