//
//  LoginViewController.swift
//  GreenHcm
//
//  Created by Fivecode on 23/09/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import Foundation
import UIKit

// MARK: - SegueConstants

enum SegueConstantLogin {
    // TODO: Add segue ids
}

// MARK: -  LoginViewController

class LoginViewController: ViewController {
    
    // MARK: Properties
    
    var presenter: LoginPresenter!
    
    // MARK: Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        LoginPresenter.config(withLoginViewController: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

// MARK: - LoginView

extension LoginViewController: LoginView {
    // TODO: implement view methods
}
