//
//  LoginPresenter.swift
//  GreenHcm
//
//  Created by Fivecode on 23/09/19.
//  Copyright © 2019 Personal. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol LoginViewPresenter: class {
    init(view: LoginView)
    // TODO: Declare view presenter methods
}

protocol LoginView: class {
    // TODO: Declare view methods
}

class LoginPresenter: LoginViewPresenter {
    
    static func config(withLoginViewController vc: LoginViewController) {
        let presenter = LoginPresenter(view: vc)
        vc.presenter = presenter
    }
    
    let view: LoginView
    
    required init(view: LoginView) {
        self.view = view
    }
    
    // TODO: Implement view presenter methods
}
