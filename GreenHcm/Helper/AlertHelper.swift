//
//  AlertHelper.swift
//  Mabar
//
//  Created by Reza Ilham on 20/06/19.
//  Copyright © 2019 Mabar. All rights reserved.
//

import UIKit

class AlertHelper {
    
    public func showBasicAlert(_ title: String?, message: String, controller: UIViewController) {
        let alert: UIAlertController = AlertHelper().createBasicAlert(
            title
            ,message: message
            ,titleButton: "OK"
        )
        controller.present(alert, animated: true, completion: nil)
    }
    
    public func showBasicAlert(_ title: String?, message: String, titleButton: String, controller: UIViewController) {
        let alert: UIAlertController = AlertHelper().createBasicAlert(
            title
            ,message: message
            ,titleButton: titleButton
        )
        controller.present(alert, animated: true, completion: nil)
    }
    
    public func showAlert(_ title: String?, message: String, actions: [UIAlertAction], controller: UIViewController) {
        let alertController = UIAlertController(
            title: title
            ,message: message
            ,preferredStyle: UIAlertController.Style.alert
        )
        if actions.count > 0 {
            for action: UIAlertAction in actions {
                alertController.addAction(action)
            }
        }
        controller.present(alertController, animated: true, completion: nil)
    }
    
    public func createBasicAlert(_ title: String?, message: String, titleButton: String) -> UIAlertController {
        let alert = UIAlertController(
            title: title
            ,message: message
            ,preferredStyle: UIAlertController.Style.alert
        )
        alert.addAction(
            UIAlertAction(
                title: titleButton
                ,style: UIAlertAction.Style.default
                ,handler: nil
            )
        )
        return alert
    }
    
    public func fetchMessage(_ error: NSError?, statusCode: Int?) -> String {
        var message: String = ""
        if let errorCode: Int = error?.code {
            switch errorCode {
            case -1009: message = "Please check your internet connection."
            default: message = "Invalid Request."
            }
        }
        else if statusCode != nil {
            message = "Invalid Response."
        }
        return message
    }
    
}
